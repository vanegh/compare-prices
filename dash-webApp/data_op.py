from datetime import date
import datetime
import pandas as pd
import numpy as np
import dash_html_components as html
import dash_table

path = r'~/PycharmProjects/WebScraping/json_files/'

today = date.today()


# ---------------- Helper functions -------------------------------------
def find_date(date1, day):
    """Function that gets the date of the days of the week Tuesday and Wednesday."""
    # print(date.strftime("%A"))
    # Find Week number and year
    if (date1.strftime("%A") == 'Monday') or (date1.strftime("%A") == 'Tuesday' and day == "Wednesday"):
        week_number = date1.strftime("%W")  # Monday is the first day of the week
        week_number = str(int(week_number) - 1)

    else:
        week_number = date1.strftime("%W")  # Monday is the first day of the week
    # year
    year = date1.strftime("%Y")
    print(week_number, year)
    # Find the date for this week's Tuesday
    week_year = year + '-W' + week_number
    if day == "Tuesday":
        shift = '-2'
    else:
        shift = '-3'
    date_day = datetime.datetime.strptime(week_year + shift, "%Y-W%W-%w")
    date_day = date_day.strftime("%A_%d_%m_%Y")
    print(date_day)
    return date_day


def get_file(date1, day, store):
    """Function that get the filename where the prices are"""
    print(date1.strftime("%A_%d_%m_%Y"), day)
    if (date1.strftime("%A") == "Tuesday" and day == date1.strftime("%A")) or (
            date1.strftime("%A") == "Wednesday" and day == date1.strftime("%A")):
        print("bu")
        date_day = date1.strftime("%A_%d_%m_%Y")

    else:
        date_day = find_date(today, day)

    file_name = store + 'FyV_' + date_day + '.json'
    print(path + file_name)
    return pd.read_json(path + file_name)


def get_prices_soriana_tuesday():
    df1 = get_file(today, "Tuesday", "soriana")
    # remove duplicates
    df1 = df1.drop_duplicates(df1)
    return display_table_tue(df1)


def get_prices_soriana_tue_wed():
    # Soriana
    df1 = get_file(today, "Tuesday", "soriana")
    df2 = get_file(today, "Wednesday", "soriana")
    # remove duplicates
    df1 = df1.drop_duplicates(df1)
    df2 = df2.drop_duplicates(df2)
    # number of rows
    # print(df1.shape[0], df2.shape[0])
    # Merge Soriana Data
    if df2.shape[0] > df1.shape[0]:
        results = df1.merge(df2, on='Item', how='right')
    else:
        results = df1.merge(df2, on='Item')

    fecha1 = 'Precio ' + df1.iloc[0]['Fecha consulta']
    fecha2 = 'Precio ' + df2.iloc[0]['Fecha consulta']
    # print(fecha1, fecha2)

    # remove columns of precio anterior
    # df = df.drop(df.columns[[0, 1, 3]], axis=1)
    results = results.drop(results.columns[[0, 3, 4, 5, 7]], axis=1)

    # rename columns
    results.columns = ['Item', fecha1, fecha2, 'Tienda']
    return display_data_tue_wed(results), results, fecha1, fecha2


def get_changed_products():
    results = get_prices_soriana_tue_wed()[1]
    fecha1 = get_prices_soriana_tue_wed()[2]
    fecha2 = get_prices_soriana_tue_wed()[3]
    # Items that changed
    items_changed_price = results[results[fecha1] != results[fecha2]]
    return display_data(items_changed_price)


def get_prices_la_comer():
    df1 = get_file(today, "Wednesday", "comer")
    # remove duplicates
    df1 = df1.drop_duplicates(df1)
    # filter Frutas, Verduras and Frutas cítricas
    fyv1 = df1[
        (df1['Categoria'] == "Frutas") | (df1['Categoria'] == "Verduras") | (df1['Categoria'] == 'Frutas cítricas')]

    fyv1 = fyv1.drop(fyv1.columns[1], axis=1)
    return display_table_tue(fyv1)


def get_compared_prices():
    # List of items
    fyv = ['Betabel', 'Calabaza Criolla|Calabaza Italiana', 'Chayote S/Espinas|Sin Espinas', 'Durazno', 'Guayaba', 'Jicama',
           'Jitomate Saladet|Bola|Huaje', 'Lechuga Romana',
           'Mamey', 'Mango Ataulfo', 'Manzana Golden', 'Manzana Royal Gala', 'Manzana Red', 'Melon Chino', 'Naranja',
           'Nopal', 'Papaya|Maradol',
           'Platano',
           'Tomatillo', 'Toronja', 'Tuna Blanca', 'Zanahoria', 'Chile Arbol', 'Chile Manzano', 'Chile Poblano',
           'Col Blanca', 'Col Morada', 'Papa Blanca', 'Pepino', 'Platano Macho']

    # Soriana
    df1 = get_file(today, "Wednesday", "soriana")
    # La Comer
    df2 = get_file(today, "Wednesday", "comer")

    # remove duplicates
    df1 = df1.drop_duplicates(df1)
    df2 = df2.drop_duplicates(df2)

    # La Comer
    # filter Frutas, Verduras and Frutas cítricas
    fyv1 = df2[
        (df2['Categoria'] == "Frutas") | (df2['Categoria'] == "Verduras") | (df2['Categoria'] == 'Frutas cítricas')]

    # Change to title style
    fyv1.loc[:, 'Item'] = fyv1['Item'].str.title()

    fyv1 = fyv1.drop(fyv1.columns[[0, 1]], axis=1)

    # Concatenate df of Soriana and La Comer
    # double_precip = pd.concat([precip_one_station, precip_one_station])
    so_co = pd.concat([df1, fyv1])
    so_co = so_co.drop_duplicates(so_co)

    fyv_list = so_co[so_co['Item'].str.contains('|'.join(fyv))]
    # sort
    fyv_list = fyv_list.sort_values(by='Item')
    tables_div = []
    for item in fyv:
        tables_div.append(display_compared_products(fyv_list[fyv_list['Item'].str.contains(item)], item))

    return tables_div


# Tables functions

# return html Table with dataframe values
def df_to_table(df):
    """Return html Table with Html values"""
    return html.Table(
        [html.Tr([html.Th(col) for col in df.columns])]
        + [
            html.Tr([html.Td(df.iloc[i][col]) for col in df.columns])
            for i in range(len(df))
        ]
    )


def display_data_tue_wed(df):
    return dash_table.DataTable(
        columns=[{"name": i, "id": i} for i in df.columns],
        data=df.to_dict('records'),
        fixed_rows={'headers': True},
        style_table={'height': 400, 'width': '70%'},
        style_data={'border': '2px solid white'},
        style_cell={
            'minWidth': 95, 'maxWidth': 95, 'width': 95, 'textAlign': 'left', 'fontSize': 14
        },
        style_header={
            'backgroundColor': '#3F826D',
            'fontWeight': 'bold',
            'border': '2px solid white',
            'fontSize': 16,
            'color': '#E3E4DB'
        },
        style_data_conditional=[
            {
                'if': {'row_index': 'even'},
                'backgroundColor': '#E3E4DB'
            },
            {
                'if': {'column_id': 'Tienda'},
                'width': '10%'
            },
            {
                'if': {'column_id': 'Item'},
                'width': '40%'
            }
        ]
    )


def display_data(df):
    fecha1 = get_prices_soriana_tue_wed()[2]
    fecha2 = get_prices_soriana_tue_wed()[3]
    return dash_table.DataTable(
        columns=[{"name": i, "id": i} for i in df.columns],
        data=df.to_dict('records'),
        fixed_rows={'headers': True},
        style_table={'height': 400, 'width': '70%'},
        style_data={'border': '2px solid white'},
        style_cell={
            'minWidth': 95, 'maxWidth': 95, 'width': 95, 'textAlign': 'left', 'fontSize': 14
        },
        style_header={
            'backgroundColor': '#CDCDCD',
            'fontWeight': 'bold',
            'border': '2px solid white',
            'fontSize': 16
        },
        style_data_conditional=[
            {
                'if': {'row_index': 'odd'},
                'backgroundColor': '#E3E4DB'
            },
            {
                'if': {
                    'filter_query': '{' + f'{fecha1}' + '}' + '<' + '{' + f'{fecha2}' + '}',
                    'column_id': f'{fecha1}'
                },
                'backgroundColor': '#92B6B1',
                'color': 'white'

            },
            {
                'if': {
                    'filter_query': '{' + f'{fecha2}' + '}' + '<' + '{' + f'{fecha1}' + '}',
                    'column_id': f'{fecha2}'
                },
                'backgroundColor': '#92B6B1',
                'color': 'white'

            },
            {
                'if': {'column_id': 'Tienda'},
                'width': '10%'
            },
            {
                'if': {'column_id': 'Item'},
                'width': '40%'
            }

        ]

    )


def display_table_tue(df):
    return dash_table.DataTable(
        columns=[{'id': i, 'name': i} for i in df.columns],
        data=df.to_dict('records'),
        style_table={'height': 400, 'width': '70%'},
        page_size=10,
        style_data={'border': '2px solid white'},
        style_cell={
            'minWidth': 95, 'maxWidth': 95, 'width': 95, 'textAlign': 'left', 'fontSize': 14
        },
        style_header={
            'backgroundColor': '#CDCDCD',
            'fontWeight': 'bold',
            'border': '2px solid white',
            'fontSize': 16
        },
        style_data_conditional=[
            {
                'if': {'row_index': 'odd'},
                'backgroundColor': '#E3E4DB'
            },
            {
                'if': {'column_id': 'Tienda'},
                'width': '10%'
            },
            {
                'if': {'column_id': 'Fecha consulta'},
                'width': '15%'
            },
            {
                'if': {'column_id': 'Precio actual'},
                'width': '15%'
            },
            {
                'if': {'column_id': 'Precio anterior'},
                'width': '17%'
            },
            {
                'if': {'column_id': 'Item'},
                'width': '40%'
            }
        ]
    )


def display_compared_products(df, item):
    return html.Div([
        html.H2(item),
        dash_table.DataTable(
            columns=[{'id': i, 'name': i} for i in df.columns],
            data=df.to_dict('records'),
            style_table={'height': 400, 'width': '70%'},
            style_data={'border': '2px solid white'},
            style_cell={
                'minWidth': 95, 'maxWidth': 95, 'width': 95, 'textAlign': 'left', 'fontSize': 14
            },
            style_header={
                'backgroundColor': '#CDCDCD',
                'fontWeight': 'bold',
                'border': '2px solid white',
                'fontSize': 16
            },
            style_data_conditional=[
                {
                    'if': {'row_index': 'odd'},
                    'backgroundColor': '#E3E4DB'
                },
                {
                    'if': {'column_id': 'Tienda'},
                    'width': '10%'
                },
                {
                    'if': {'column_id': 'Fecha consulta'},
                    'width': '15%'
                },
                {
                    'if': {'column_id': 'Precio actual'},
                    'width': '15%'
                },
                {
                    'if': {'column_id': 'Precio anterior'},
                    'width': '17%'
                },
                {
                    'if': {'column_id': 'Item'},
                    'width': '40%'
                },
                {
                    'if': {
                        'filter_query': '{{Precio actual}} = {}'.format(df['Precio actual'].min()),

                    },
                    'backgroundColor': '#266DD3',
                    'color': 'white'
                },


            ]
        )

    ])
