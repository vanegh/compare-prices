import dash_core_components as dcc
import dash_html_components as html
import pandas as pd
from dash.dependencies import Input, Output
from data_op import get_prices_soriana_tuesday, get_prices_soriana_tue_wed, get_changed_products
from app import app

path = r'~/PycharmProjects/WebScraping/json_files/'

layout = [
    html.Div([
        html.Div(
            className="row header",
            children=[
                html.Span(
                    className="app-title",
                    children=[
                        dcc.Markdown("**Precios de frutas y verduras en Soriana **"),
                        html.Span(id="subtitle", children=dcc.Markdown("&nbsp Martes y Miércoles"),
                                  # style={"font-size": "1.8rem","margin-top":"15px"},
                                  ),
                    ],
                )]
        )
    ]),
    html.Div(html.H1("Martes")),
            html.Div(id="leads_table",
                     className="row pretty_container table",
                     children=get_prices_soriana_tuesday()
                     ),
    html.Div(html.H1("Martes vs. Miércoles")),
            html.Div(id="leads_table",
                     className="row pretty_container table",
                     children=get_prices_soriana_tue_wed()[0]

                     ),
    html.Div(html.H1("Productos que cambiaron de precio")),
            html.Div(
                id="leads_table",
                className="row pretty_container table",
                children=get_changed_products())

]


# ---------------------------- Callbacks --------------------------------------

@app.callback(Output('tabs-content-props', 'children'),
              [Input('tabs-styled-with-props', 'value')])
def render_content(tab):
    if tab == 'tab-1':
        print('1')
        return html.Div([

        ])
    elif tab == 'tab-2':
        print('2')
        return html.Div([

        ])
    elif tab == 'tab-3':
        return html.Div([


        ])


