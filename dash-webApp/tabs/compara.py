import dash_core_components as dcc
import dash_html_components as html
from data_op import get_compared_prices

layout = [
    html.Div([
        html.Div(
            className="row header",
            children=[
                html.Span(
                    className="app-title",
                    children=[
                        dcc.Markdown("** Compara precios de frutas y verduras en Soriana y la Comer **"),
                        html.Span(id="subtitle", children=dcc.Markdown("&nbsp Miércoles"))
                    ]
                )
            ]
        )
    ]),
    html.Div(html.H1(" Compara productos mas comprados por el usuario")),
    html.Div(id="leads_table",
                     className="row pretty_container table",
                     children=get_compared_prices()

                     )


]
