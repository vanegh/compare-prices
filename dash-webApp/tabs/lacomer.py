import dash_core_components as dcc
import dash_html_components as html
import pandas as pd
from data_op import get_prices_la_comer

layout = [
    html.Div([
        html.Div(
            className="row header",
            children=[
                html.Span(
                    className="app-title",
                    children=[
                        dcc.Markdown("** Precios de frutas y verduras en la Comer **"),
                        html.Span(id="subtitle", children=dcc.Markdown("&nbsp Miércoles"))
                    ]
                )
            ]
        )
    ]),
    html.Div(html.H1("Miércoles")),
    html.Div(
        id="leads_table",
        className="row pretty_container table",
        children=get_prices_la_comer())

]
