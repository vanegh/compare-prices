import dash
import dash_core_components as dcc
import dash_html_components as html
from dash.dependencies import Input, Output, State
from tabs import soriana, lacomer, compara
path = r'~/PycharmProjects/WebScraping/json_files/'


external_stylesheets = ['https://codepen.io/chriddyp/pen/bWLgP.css']
app = dash.Dash(__name__)

app.layout = html.Div([
    html.Div(
        className="row header",
        children=[
            html.Span(className="app-title",
                      children=[
                          dcc.Markdown("**Compara Precios App**"),
                          html.Span(id="subtitle",
                                    children=dcc.Markdown("&nbsp Soriana y la Comer "),
                                    style={"font-size": "1.8rem","margin-top":"15px"},
                                    ),
                      ],
                      )
        ]
    ),
    html.Div(
        id="tabs",
        className="row tabs",
        children=[
            dcc.Link("Soriana", href="/"),
            dcc.Link("La Comer", href="/"),
            dcc.Link("Compara Precios", href="/")
        ]
    ),
    html.Div(
        id="mobile_tabs",
        className="row tabs",
        style={"display": "none"},
        children=[
            dcc.Link("Soriana", href="/"),
            dcc.Link("La Comer", href="/"),
            dcc.Link("Compara Precios", href="/")
        ]
    ),
    dcc.Location(id="url", refresh=False),
    html.Div(id="tab_content")
],
    className="row",
    style={"margin": "0%"},
)

#update the index

@app.callback([
    Output("tab_content", "children"),
    Output("tabs", "children"),
    Output("mobile_tabs", "children"),
],
    [Input("url", "pathname")]
)
def display_page(pathname):
    tabs=[
        dcc.Link("Soriana", href="/dash-webApp/soriana"),
        dcc.Link("La Comer", href="/dash-webApp/lacomer"),
        dcc.Link("Compara Precios", href="/dash-webApp/compara"),
    ]
    if pathname == "/dash-webApp/soriana":
        print('bu')
        tabs[0] = dcc.Link(dcc.Markdown("**&#9632 Soriana**"), href="/dash-weApp/soriana")
        return soriana.layout, tabs, tabs

    elif pathname == "/dash-webApp/lacomer":
        tabs[1] = dcc.Link(dcc.Markdown("**&#9632 La Comer**"), href="/dash-webApp/lacomer")
        return lacomer.layout, tabs, tabs
    tabs[2] = dcc.Link(dcc.Markdown("**&#9632 Compara Precios**"), href="/dash-webApp/compara")
    return compara.layout, tabs, tabs



if __name__ == '__main__':
    app.run_server(debug= True)