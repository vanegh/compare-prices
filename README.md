## Compare Prices

**Compare Prices** is a personal project whose purpose is to compare prices of Fruits and Vegetables sold at two different stores (Soriana and La Comer)
that have special offers on Tuesdays and Wednesdays. 

Prices of Soriana on Tuesday and Wednesday are compared to see the variations in between days. Then, prices on Wednesdays are
compared between the stores Soriana and la Comer.

This project has three completed parts:

1. Web Scraping

2. Web App with Flask (See Folder /webAppFlask)

3. Web App with Dash (See folder /dash-webApp)

4. Chatbot with Telegram (Under construction) - Added as a feature

5. Dockerize (Future)


It is worth to mention that most of my personal projects has the intention of solving a problem, automate stuff, 
and last but not least, to learn. So sometimes I will take the long path to learn a module or a technology and 
probably it would not be the most efficient solution.

---

## 1. Web Scraping

Here, I do some **Web scraping** in the Web pages of the stores to get the prices. To carry out this, I use two techniques 
that I found in a great book called **"Web scraping with python"** by Ryan Mitchell. 

For La Comer store, I used **beautifulsoup module** and created a bunch of functions. Want to see the code? See *Scrapcomer.py*

For Soriana store, I digged more into the book and found a special Web scraping framework called **scrapy**. Go to */spiders*

Now, the collected data is saved in json files. As a further step, the data will be saved in a database **MongoDB**.

These two python scripts are run periodically (Tuesdays and Wednesdays) through **crontab** and shell scripts. See *run_soriana_spider.sh* and *run_laComer.sh*

---

## 2. Web app with Flask

To visualize the collected data, I created a Web app with Flask that has three tabs. All the data is manipulated through **pandas**.
The web App consists of three different tabs.

*Martes* tab has the prices of Soriana on Tuesday:

![Soriana-Tuesdays](images/Soriana-Tuesdays.png)

*Miercoles* is a dropdown tab with two pages: *Soriana* and *la Comer*

Soriana shows two tables: 

*Precios Martes y Miercoles (Prices on Tuesday and Wednesday)*:

Here a table where the current prices are shown in a way to compare them.

![Soriana-Tue-Wed](images/Soriana-Tue-Wed.png)


*Productos que cambiaron de precio (Prices that changed)* :
A table where ...

![Soriana-Tue-Wed-Variations](images/Soriana-Tue-Wed-Variations.png)


*Compara precios* Comparing prices between 

![Soriana-Comer](images/Soriana-Comer.png)



---
## 3. Web app with Dash 

Recently, I found another way to visualize data: **Dash** and I decided to give it a try. According to its Web Page, "**[Dash](https://dash.plotly.com)** is a productive Python framework for building web applications".
At first, I was editing one of the [demo apps](https://dash-gallery.plotly.host/dash-salesforce-crm/) found on Dash site. Then I started to modify this templates according to my needs and 
this is what I got:

The web App has three tabs: Soriana, la Comer and Compara precios.

**1. Soriana tab**. 
   It shows three tables: the first one shows the prices on Tuesday.
   
   ![SorianaTab1](images/SorianaTab1.png)
   
   The second one is a comparative table showing the prices and a third table displays the products that changed their price.
   
   ![SorianaTab2](images/SorianaTab2.png)
   
**2. La Comer tab.** 
   It shows the prices of products on Wednesday.

   ![LaComerTab](images/LaComerTab.png)
   
**3. Compara Precios tab.**
   Different tables with the most bought products and their prices are compared between Soriana and La Comer. 
   The cheapest price is highlighted in blue.
   
   ![ComparaPreciosTab](images/ComparaPreciosTab.png)
   
   


---
## 4. Chatbot with Telegram

Now, it is just an idea. I hope soon I will start to  develop it :)
