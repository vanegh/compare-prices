from datetime import date
from flask import render_template
from flaskr import app
import datetime
import pandas as pd
import numpy as np

path = r'~/PycharmProjects/WebScraping/json_files/'

today = date.today()


# ---------------- Helper functions -------------------------------------
def find_date(date1, day):
    """Function that gets the date of the days of the week Tuesday and Wednesday."""
    # print(date.strftime("%A"))
    # Find Week number and year
    if (date1.strftime("%A") == 'Monday') or  (date1.strftime("%A") == 'Tuesday' and day == "Wednesday"):
        week_number = date1.strftime("%W")  # Monday is the first day of the week
        week_number = str(int(week_number) - 1)

    else:
        week_number = date1.strftime("%W")  # Monday is the first day of the week
    # year
    year = date1.strftime("%Y")
    print(week_number, year)
    # Find the date for this week's Tuesday
    week_year = year + '-W' + week_number
    if day == "Tuesday":
        shift = '-2'
    else:
        shift = '-3'
    date_day = datetime.datetime.strptime(week_year + shift, "%Y-W%W-%w")
    date_day = date_day.strftime("%A_%d_%m_%Y")
    print(date_day)
    return date_day


def get_file(date1, day, store):
    """Function that get the filename where the prices are"""
    print(date1.strftime("%A_%d_%m_%Y"), day)
    if (date1.strftime("%A") == "Tuesday" and day == date1.strftime("%A")) or (date1.strftime("%A") == "Wednesday" and day == date1.strftime("%A") ):
        print("bu")
        date_day = date1.strftime("%A_%d_%m_%Y")

    else:
        date_day = find_date(today, day)

    file_name = store + 'FyV_' + date_day + '.json'
    print(path + file_name)
    return pd.read_json(path + file_name)


@app.route('/')
@app.route('/index')
def index():
    return render_template('index.html', title='Home')


@app.route('/martes')  # Show prices of Soriana store
def show_table():
    df1 = get_file(today, "Tuesday", "soriana")
    # remove duplicates
    df1 = df1.drop_duplicates(df1)
    return render_template('view.html',
                           tables=[df1.to_html(classes='martes')],
                           titles=['na', 'Precios Frutas y Verduras'],
                           title=df1.iloc[0]['Tienda'] + ' ' + df1.iloc[0]['Fecha consulta'])


@app.route('/soriana')
def show_tables():
    # Soriana
    df1 = get_file(today, "Tuesday", "soriana")
    df2 = get_file(today, "Wednesday", "soriana")
    # remove duplicates
    df1 = df1.drop_duplicates(df1)
    df2 = df2.drop_duplicates(df2)
    # number of rows
    # print(df1.shape[0], df2.shape[0])
    # Merge Soriana Data
    if df2.shape[0] > df1.shape[0]:
        results = df1.merge(df2, on='Item', how='right')
    else:
        results = df1.merge(df2, on='Item')

    fecha1 = 'Precio ' + df1.iloc[0]['Fecha consulta']
    fecha2 = 'Precio ' + df2.iloc[0]['Fecha consulta']
    # print(fecha1, fecha2)

    # remove columns of precio anterior
    # df = df.drop(df.columns[[0, 1, 3]], axis=1)
    results = results.drop(results.columns[[0, 3, 4, 5, 7]], axis=1)

    # rename columns
    results.columns = ['Item', fecha1, fecha2, 'Tienda']

    # Items that changed
    items_changed_price = results[results[fecha1] != results[fecha2]]

    # highlight the ones with the lowest price
    # noinspection PyShadowingNames
    def highlight_cheapest_price(x):
        r = 'red'
        y = 'yellow'

        m1 = x[fecha1] < x[fecha2]
        m2 = x[fecha2] < x[fecha1]

        df1 = pd.DataFrame('background-color: ', index=x.index, columns=x.columns)
        # rewrite values
        df1[fecha1] = np.where(m1, 'background-color: {}'.format(r), df1[fecha1])
        df1[fecha2] = np.where(m2, 'background-color: {}'.format(y), df1[fecha2])
        return df1

    items_changed_price = items_changed_price.style.apply(highlight_cheapest_price, axis=None)

    return render_template('view.html',
                           tables=[results.to_html(classes='compara'), items_changed_price.render(classes='cambios')],
                           titles=['na', 'Compara precios', 'Productos que cambiaron de precio'],
                           title=df1.iloc[0]['Tienda'] + ' Martes ' + df1.iloc[0]['Fecha consulta'] + ' y Miercoles ' + df2.iloc[0][
                               'Fecha consulta'])


@app.route('/lacomer')
def show():
    # La Comer
    df1 = get_file(today, "Wednesday", "comer")
    # remove duplicates
    df1 = df1.drop_duplicates(df1)
    # filter Frutas, Verduras and Frutas cítricas
    fyv1 = df1[
        (df1['Categoria'] == "Frutas") | (df1['Categoria'] == "Verduras") | (df1['Categoria'] == 'Frutas cítricas')]

    fyv1 = fyv1.drop(fyv1.columns[1], axis=1)
    return render_template('view.html',
                           tables=[fyv1.to_html(classes='lacomer')],
                           titles=['na', 'Precios Frutas y Verduras'],
                           title=fyv1.iloc[0]['Tienda'] + ' ' + df1.iloc[0]['Fecha consulta'])


@app.route('/compara')
def compare():
    # List of items
    fyv = ['Betabel', 'Calabaza Criolla|Italiana', 'Chayote S/Espinas|Sin Espinas', 'Durazno', 'Guayaba', 'Jicama',
           'Jitomate Saladet|Bola|Huaje', 'Lechuga Romana',
           'Mamey', 'Mango Ataulfo', 'Manzana Golden', 'Manzana Royal Gala', 'Manzana Red', 'Melon Chino', 'Naranja',
           'Nopal', 'Papaya|Maradol',
           'Platano',
           'Tomatillo', 'Toronja', 'Tuna Blanca', 'Zanahoria', 'Chile Arbol', 'Chile Manzano', 'Chile Poblano',
           'Col Blanca', 'Col Morada', 'Papa Blanca', 'Pepino', 'Platano Macho']
    # Soriana
    df1 = get_file(today, "Wednesday", "soriana")
    # La Comer
    df2 = get_file(today, "Wednesday", "comer")

    # remove duplicates
    df1 = df1.drop_duplicates(df1)
    df2 = df2.drop_duplicates(df2)

    # La Comer
    # filter Frutas, Verduras and Frutas cítricas
    fyv1 = df2[
        (df2['Categoria'] == "Frutas") | (df2['Categoria'] == "Verduras") | (df2['Categoria'] == 'Frutas cítricas')]

    # Change to title style
    fyv1.loc[:, 'Item'] = fyv1['Item'].str.title()

    fyv1 = fyv1.drop(fyv1.columns[[0, 1]], axis=1)

    # Concatenate df of Soriana and La Comer
    # double_precip = pd.concat([precip_one_station, precip_one_station])
    so_co = pd.concat([df1, fyv1])
    so_co = so_co.drop_duplicates(so_co)

    # Find
    # df[(df['count'].isin([2,7])) & (df['price'].isin([100, 221]))]

    fyv_list = so_co[so_co['Item'].str.contains('|'.join(fyv))]
    # sort
    # fyv_list.sort_values(by='Item')
    # tables=[fyv_list.sort_values(by='Item').to_html(classes='compara')]
    return render_template('compara.html', data=fyv_list.sort_values(by='Item'),
                           titles=fyv)