from celery import Celery
from celery.schedules import crontab
from ScrapComer import get_products
import os


celery = Celery('webscraping', broker="amqp://guest:guest@127.0.0.1:5672//")
# disable UTC so that Celery can use local time
celery.conf.enable_utc = False


@celery.task
def get_la_comer_prices():
    print('La Comer prices')
    url_head = 'https://www.lacomer.com.mx/lacomer/'
    departamentos = {
        'Frutas y Verduras': 'doHome.action?idDepartamentoHidden=13&key=Frutas-y-Verduras&succId=287&opcion=pasillos'
                             '&agruId=13&succFmt=100'}
    file_name= get_products(url_head + departamentos['Frutas y Verduras'])
    print(file_name)
    #return file_name

@celery.task
def get_soriana_prices():
    print("Soriana prices")
    os.system('cd ~/pycharmprojects/WebScraping/scrapy/sorianaSpider/sorianaSpider/spiders && sh run_soriana_spider.sh')

# add get_la_comer_prices task to the beat schedule
celery.conf.beat_schedule = {
    'add-la-comer-prices': {
        'task': 'webscraping.get_la_comer_prices',
        'schedule': crontab(hour=18, minute=32, day_of_week=[0, 6])
    },
    'add-soriana-prices': {
        'task': 'webscraping.get_soriana_prices',
        'schedule': crontab(hour=18, minute=35, day_of_week=[0,6])
    }
}

