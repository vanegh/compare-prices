from urllib.request import Request, urlopen
from urllib.error import HTTPError
from bs4 import BeautifulSoup
from itertools import zip_longest
from datetime import date
import json
import re
import urllib.parse

urlHead = 'https://www.lacomer.com.mx/lacomer/'
item, precioActual, precioAnterior, products, categorias = [], [], [], [], []

today = date.today()
filename = '/Users/vanesagarridoh/PycharmProjects/WebScraping/json_files/comerFyV_' + today.strftime("%A_%d_%m_%Y") + '.json'

departamentos = {
    "Frutas y Verduras": "doHome.action?idDepartamentoHidden=13&key=Frutas-y-Verduras&succId=287&opcion=pasillos&agruId=13&succFmt=100"}


# products = get_products(urlHead+departamentos['Frutas y Verduras'])
def price_2_float(price):
    price = re.sub('M.N.', '', price)
    price = re.sub('[^0-9.]', '', price)
    return float(price)


def save_2_file(products):
    with open(filename, 'w', encoding="utf-8") as myfile:
        json.dump(products, myfile, ensure_ascii=False, indent=4)
    myfile.close()


def getDepartmentCategoriesPagesLinks(url):
    bsObj = getBSObject(url)
    paginator = bsObj.find("div", {"class": "paginator-container"}).findAll("a")
    # print(paginator)

    # Getting the link of the pages where to scrap
    links = [link.attrs['href'] for link in paginator if
             link.get_text() != 'Siguiente' and link.attrs['href'] != '#']
    print(len(links))
    return links


def getDepartmentCategoriesProducts(link, category):
    bsObj = getBSObject(link)
    nameProductList = bsObj.findAll("div", {"class": "li_prod_nombre middle-in"})
    priceProductList = bsObj.findAll("div", {"class": "li_prod_precio cgrid-price list-price"})
    for nameProduct in nameProductList:
        item.append(nameProduct.div.a.get_text().title().strip('\n'))
        categorias.append(category)
        # print(nameProduct.div.a.get_text())
        # print(nameProduct.div.p.get_text())

    for priceProduct in priceProductList:
        cur_price = price_2_float(priceProduct.find('span', {'class': 'precio_normal'}).text)
        precioActual.append(cur_price)
        # print(priceProduct.find('span', {'class': 'precio_normal'}).text)
        if priceProduct.find('span', {'class': 'precio_old'}) == None:
            precioAnterior.append(cur_price)
            # print("Sin cambio")
        else:
            # print(priceProduct.find('span', {'class': 'precio_old'}).text)
            pre_price = price_2_float(priceProduct.find('span', {'class': 'precio_old'}).text)
            precioAnterior.append(pre_price)
    #print(len(item))
    # print(len(precioActual))
    # print(len(precioAnterior))
    products = [{"Item": i, "Precio anterior": ant, "Precio actual": act, "Categoria": cat} for i, ant, act, cat in
                zip(item, precioAnterior, precioActual, categorias)]
    today = date.today()
    for i in products:
        i.update(
            {'Departamento': 'Frutas y Verduras', 'Tienda': 'La Comer', 'Fecha consulta': today.strftime("%d-%m-%y")})
    # products = json.dumps(products)
    save_2_file(products)
    return products


def getDepartmentCategoriesLinks(bsObj):
    """Returns a dictionary with the name of the categories found per department and their links"""
    # print(bsObj.prettify())
    categories = bsObj.findAll("div", {"class": "col-lg-4 col-md-6 col-sm-12"})
    # print(categories)
    keys = [re.sub(r'\([^)]*\)', '', x.a.get_text()).strip() for x in categories]
    category = {x: [y.a.attrs['href']] for x, y in zip(keys, categories)}
    # print(keys)
    return category


def getBSObject(url):
    try:
        request = Request(url, headers={'User-Agent': 'Mozilla/5.0'})
        html = urlopen(request).read()

    except HTTPError as e:
        return None

    try:
        bsObj = BeautifulSoup(html, "html.parser")
        return bsObj
    except AttributeError as e:
        return None


def get_products(url):
    bsObj = getBSObject(url)
    categoriesLinks = getDepartmentCategoriesLinks(bsObj)
    for k, v in categoriesLinks.items():
        #print(urlHead + v[0])
        links = getDepartmentCategoriesPagesLinks(urlHead + urllib.parse.quote(v[0]))
        categoriesLinks[k] = v + links
        for link in categoriesLinks[k]:
            #print(k)
            products = getDepartmentCategoriesProducts(urlHead + urllib.parse.quote(link), k)
    return filename

#get_products(urlHead+departamentos['Frutas y Verduras'])
#print(filename)
