from celery import Celery
from celery.schedules import crontab

celery = Celery()
celery.conf.enable_utc = False
@celery.on_after_configure.connect
def setup_periodic_tasks(sender, **kwargs):
    sender.add_periodic_task(crontab(hour=22, minute=18, day_of_week=[0,5,6]), test.s('hallo'),
                             )



@celery.task
def test(arg):
    print(arg)