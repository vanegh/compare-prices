from celery.schedules import crontab

broker_url = 'amqp://guest:guest@127.0.0.1:5672//'

task_serializer = 'json'
result_serializer = 'json'
accept_content = ['json']

beat_schedule = {
    # Executes every Tuesday and Wednesday at 8:00 a.m.
    'add_soriana_prices': {
        'task': 'micro_page.get_soriana_prices',
        'schedule': crontab(hour=8, minute=1, day_of_week='tuesday,wednesday'),
    },
    # Executes every Wednesday at 8:05 a.m.
    'add_la_comer_prices': {
        'task': 'micro_page.get_la_comer_prices',
        'schedule': crontab(hour=22, minute=50, day_of_week='thursday'),
    }
}
