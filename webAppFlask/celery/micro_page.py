import os
from flask import Flask,request, render_template, session, flash, redirect, url_for, jsonify
from celery import Celery
import celeryconfig
from ScrapComer import get_products
import pandas as pd



app = Flask(__name__)
app.config['SECRET_KEY'] = 'top-secret!'

#--------------------------------- Flask ------------------------------------------------
@app.route('/')
@app.route('/index')
def index():
    return render_template('index.html', title='Home')


@app.route('/martes') # Show prices of Soriana
def show_table():
    df1 = pd.read_json(r'~/PycharmProjects/WebScraping/json_files/sorianaFyV_Tuesday_14_07_2020.json')
    # remove duplicates
    df1 = df1.drop_duplicates(df1)
    return render_template('view.html',
                           tables=[df1.to_html(classes='martes')],
                           titles=['na', 'Precios Frutas y Verduras'],
                           title=df1.iloc[0]['Tienda'] + ' ' + df1.iloc[0]['Fecha consulta'])


@app.route('/lacomer')
def show():
    # La Comer
    #file_name = get_la_comer_prices()
    df1 = pd.read_json(file_name) #r'~/PycharmProjects/WebScraping/json_files/comerFyV_Wednesday_15_07_20.json'
    # remove duplicates
    df1 = df1.drop_duplicates(df1)
    # filter Frutas, Verduras and Frutas cítricas
    fyv1 = df1[
        (df1['Categoria'] == "Frutas") | (df1['Categoria'] == "Verduras") | (df1['Categoria'] == 'Frutas cítricas')]

    fyv1 = fyv1.drop(fyv1.columns[1], axis=1)
    return render_template('view.html',
                           tables=[fyv1.to_html(classes='lacomer')],
                           titles=['na', 'Precios Frutas y Verduras'],
                           title=fyv1.iloc[0]['Tienda'] + ' ' + df1.iloc[0]['Fecha consulta'])

#--------------------------------- Celery ------------------------------------------------
def make_celery(app):
    """Setup Celery"""
    celery = Celery(app.name)
    celery.config_from_object(celeryconfig)
    class ContextTask(celery.Task):
        def __call__(self, *args, **kwargs):
            with app.app_context():
                return self.run(*args, **kwargs)
    celery.Task = ContextTask
    return celery


celery = make_celery(app)
#--------------------------------- Celery Tasks ------------------------------------------------
@celery.task
def get_la_comer_prices():
    print('La Comer prices')
    url_head = 'https://www.lacomer.com.mx/lacomer/'
    departamentos = {
        'Frutas y Verduras': 'doHome.action?idDepartamentoHidden=13&key=Frutas-y-Verduras&succId=287&opcion=pasillos'
                             '&agruId=13&succFmt=100'}
    get_products(url_head + departamentos['Frutas y Verduras'])


@celery.task
def get_soriana_prices():
    print('Soriana prices')
    os.system('cd ~/pycharmprojects/WebScraping/scrapy/sorianaSpider/sorianaSpider/spiders && sh run_soriana_spider.sh')
