import scrapy

class ProductsSpider(scrapy.Spider):
    name = "products"

    domain = " https://superentucasa.soriana.com/"
    start_urls = ['https://superentucasa.soriana.com/default.aspx?p=13215']
    handle_httpstatus_list = [302]
    departamentos = {}

    def start_requests(self):
        yield scrapy.Request(self.start_urls[0], dont_filter = True, callback = self.parse)

    def parse(self,response):
        self.logger.debug("(parse) response:status=%d, URL=%s" %(response.status, response.url))
        div = response.xpath('//div[contains(@class,"category-blurb")]')
        deptLinks = div.xpath('./a/@href')
        departamentos = {'departamentos': div.xpath('./a/h4/text()').getall()}
        yield from response.follow_all(deptLinks, callback = self.parse_dept)
        yield departamentos

        #parse departments
        #for info in div:
        #    yield{
        #        'departamento': info.xpath('./a/h4/text()').get(),
        #        'url': info.xpath('./a/@href').get(),
        #         }

    def parse_dept(self, response):
        divall = response.xpath('//div[contains(@class,"category-blurb")]')
        categories_links = divall.xpath('./a/@href')
        yield from response.follow_all(categories_links, callback = self.parse_categories)
        departamentos ={'categorias':divall.xpath('./a/h4/text()').getall() }
        #for info in divall:
        #    departamentos={
        #        'categorias': info.xpath('./a/h4/text()').getall(),
        #        'url': info.xpath('./a/@href').getall(),
        #    }
        yield departamentos
    def parse_categories(self,response):
        divall = response.xpath('//div[has-class("col-md-12", "col-sm-6", "col-xs-6")]')


        for info in divall:
            if info.xpath('./a/h4/text()').get() != None:
                yield{
                'item':info.xpath('./a/h4/text()').get(),
                'precio': info.xpath('./div[has-class("precios-plp", "plp-item-div")]').css('h4.precio-regular-plp::text').get()

                }
        pagination_links = response.xpath('//ul[@class="pagination"]/li/a[@class="numeros"]/@href')
        yield from response.follow_all(pagination_links, self.parse_categories)















