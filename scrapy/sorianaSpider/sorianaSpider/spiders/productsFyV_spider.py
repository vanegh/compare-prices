import scrapy
import re
from datetime import date

class ProductsSpider(scrapy.Spider):
    name = "frutasyverduras"

    domain = " https://superentucasa.soriana.com/"
    start_urls = ['https://superentucasa.soriana.com/default.aspx?p=13220']
    handle_httpstatus_list = [302]
    productos = {}

    def start_requests(self):
        yield scrapy.Request(self.start_urls[0], dont_filter = True, callback = self.parse)

    def parse(self,response):
        self.logger.debug("(parse) response:status=%d, URL=%s" %(response.status, response.url))
        div = response.xpath('//div[contains(@class,"category-blurb")]')
        category_links = div.xpath('./a/@href')
        departamentos = {'Categorias': div.xpath('./a/h4/text()').getall()}
        yield from response.follow_all(category_links, callback = self.parse_categories)

    def parse_categories(self,response):
        divall = response.xpath('//div[has-class("col-md-12", "col-sm-6", "col-xs-6")]')


        for info in divall:
            if info.xpath('./a/h4/text()').get() != None:
                precios = info.xpath('./div[has-class("precios-plp", "plp-item-div")]')
                if precios.xpath('./h4[has-class("precio-regular-plp")]/b/text()').get() != None:
                    precio_actual = precios.xpath('./h4[has-class("precio-regular-plp")]/b/text()').get()
                    precio_actual = re.sub('[$]','',precio_actual)
                    precio_actual = float(precio_actual)

                else:
                    precio_actual = precios.css('h4.precio-regular-plp::text').get()
                    precio_actual = re.sub('[$]','',precio_actual)
                    precio_actual =  float(precio_actual)

                precio_anterior = precios.css('h4.precio-regular-plp::text').get()
                precio_anterior = re.sub('[DEA$ ]','',precio_anterior)
                precio_anterior = float(precio_anterior)
                today = date.today()

                yield{
                'Item':info.xpath('./a/h4/text()').get(),
                'Precio anterior': precio_anterior,
                'Precio actual': precio_actual,
                'Tienda': 'Soriana',
                'Fecha consulta': today.strftime("%d-%m-%y"),
                }
        pagination_links = response.xpath('//ul[@class="pagination"]/li/a[@class="numeros"]/@href')
        yield from response.follow_all(pagination_links, self.parse_categories)















